import * as assert from "assert";

import * as vscode from "vscode";

suite("Extension Test Suite", () => {
  vscode.window.showInformationMessage("Start all tests.");

  test("Extension should be present", () => {
    assert.ok(vscode.extensions.getExtension("theaigang.code-fixer"));
  });

  test("Should be able to activate extension", async () => {
    await vscode.extensions.getExtension("theaigang.code-fixer")!.activate();
    assert.ok(true);
  });

  test("Should be able to register command extension.code-fixer.prompt", async () => {
    await vscode.commands.getCommands(true).then((commands) => {
      assert.ok(commands.includes("extension.code-fixer.prompt"));
    });
  });

  test("Should be able to register command extension.code-fixer.setToken", async () => {
    await vscode.commands.getCommands(true).then((commands) => {
      assert.ok(commands.includes("extension.code-fixer.setToken"));
    });
  });

  test("Should be able to register command extension.code-fixer.removeToken", async () => {
    await vscode.commands.getCommands(true).then((commands) => {
      assert.ok(commands.includes("extension.code-fixer.removeToken"));
    });
  });
});
