import * as fs from "fs";

import * as vscode from "vscode";

import { setLoading } from "./extension";

export async function runPrompt(
  context: vscode.ExtensionContext,
  run: (inputFile: string, instruction: string) => Promise<string>
) {
  let editor = vscode.window.activeTextEditor!;

  setLoading(context, true);

  let text = getTextSelected(editor);

  if (!text) {
      text = editor.document.getText();
  }
  console.log("got text:", text);

  let prompt = await vscode.window.showInputBox({
    placeHolder: "Ask: ",
  });

  
  if (prompt !== null && prompt !== undefined) {
    let newText = await run(text, prompt);
    if (isTextSelected(editor)) {
      console.log("New text:", newText);

      replaceSelectedText(editor, newText);
      setLoading(context, false);
      return;
    }
    console.log("New text:", newText);

    replace(editor, newText);
    setLoading(context, false);
  }
}

function isTextSelected(editor: vscode.TextEditor): boolean {
  let selection = editor.selection;

  return !selection.isEmpty;
}

function replaceSelectedText(editor: vscode.TextEditor, text: string): void {
  let selection = editor.selection;

  if (selection.isEmpty) {
    return;
  }

  editor.edit((editBuilder) => {
    editBuilder.replace(selection, text);
  });
}

function getTextSelected(editor: vscode.TextEditor): string {
  let selection = editor.selection;

  if (selection.isEmpty) {
    return "";
  }

  let text = editor.document.getText(selection);

  return text;
}

function replace(editor: vscode.TextEditor, content: string) {
  editor.edit((editBuilder) => {
    editBuilder.replace(new vscode.Position(0, 0), content);
  });
}
