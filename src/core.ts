import * as vscode from "vscode";

// eslint-disable-next-line @typescript-eslint/naming-convention
const { Configuration, OpenAIApi } = require("openai");

export function init(
  openaiKey: string
): (inputFile: string, instruction: string) => Promise<string> {
  const configuration = new Configuration({
    apiKey: openaiKey,
  });

  const openai = new OpenAIApi(configuration);

  return function run(input: string, instruction: string): Promise<string> {
    return openai
      .createEdit({
        model: "code-davinci-edit-001",
        input: input,
        instruction: instruction,
        temperature: 0.0,
      })
      .then(
        (response: { data: { choices: { text: any }[] } }) => 
          response.data.choices[0].text
      )
      .catch((error: any) => {
        const message = "Error while calling OpenAI's servers, check your connectivity, or OpenAI status page:\n" + error.message;
        vscode.window.showErrorMessage(message);
        console.error(message);
        return "";
      });
  };
}
