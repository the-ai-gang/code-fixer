// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";

import { init } from "./core";
import { runPrompt } from "./functions";
import AuthSettings from "./settings";

let codeFixerStatusBar: vscode.StatusBarItem;

// This method is called when the extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  console.log('Congratulations, your extension "code-fixer" is now active!');

  const settings = initSettings(context);

  let run:
    | ((inputFile: string, instruction: string) => Promise<string>)
    | null = null;

  async function initApiKey() {
    if (!run) {
      let openaiApiKey = await settings.getAuthData("openai_api_key");

      if (!openaiApiKey) {
        vscode.commands.executeCommand("extension.code-fixer.setToken");

        return;
      }

      run = init(openaiApiKey);
    }
  }

  let promptCommand = vscode.commands.registerCommand(
    "extension.code-fixer.prompt",
    async () => {
      await initApiKey();

      if (run !== null && run !== undefined) {
        await runPrompt(context, run);
      }
    }
  );

  // Register commands to save and retrieve token
  vscode.commands.registerCommand("extension.code-fixer.setToken", async () => {
    const apiKeyInput = await vscode.window.showInputBox({
      placeHolder: "Your OpenAI API KEY",
    });

    if (apiKeyInput) {
      await settings.storeAuthData("openai_api_key", apiKeyInput);
    }
  });

  vscode.commands.registerCommand(
    "extension.code-fixer.removeToken",
    async () => {
      await settings.removeAuthData("openai_api_key");
    }
  );

  // create a new status bar item that we can now manage
  codeFixerStatusBar = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
  context.subscriptions.push(codeFixerStatusBar);

  // register some listener that make sure the status bar 
  // item always up-to-date
  context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor((_) => updateStatusBarItem(context)));
  context.subscriptions.push(vscode.window.onDidChangeTextEditorSelection((_) => updateStatusBarItem(context)));

  context.subscriptions.push(promptCommand);
}

// This method is called when your extension is deactivated
export function deactivate() {}

  
function initSettings(context: vscode.ExtensionContext) {
  AuthSettings.init(context);
  return AuthSettings.instance;
}

export function setLoading(context: vscode.ExtensionContext, state: boolean) {
  context.workspaceState.update("code-fixer.loading", state);
  updateStatusBarItem(context);
}

export function getLoading(context: vscode.ExtensionContext) : boolean {  
  if (context.workspaceState.get("code-fixer.loading") === true) {
    return true;
  } else {
    return false;
  }
}

function updateStatusBarItem(context: vscode.ExtensionContext): void {
  codeFixerStatusBar.show();
  codeFixerStatusBar.color = "white";
  const idleText = `$(notebook-state-success) Code Fixer`;
  codeFixerStatusBar.text = idleText;
  if (getLoading(context)) {
    codeFixerStatusBar.text = `$(loading) Code Fixer`;
    codeFixerStatusBar.color = "orange";

  } else {
    codeFixerStatusBar.text = idleText;
  }
}