import { ExtensionContext, SecretStorage } from "vscode";

export default class AuthSettings {
    private static _instance: AuthSettings;

    constructor(private secretStorage: SecretStorage) {}

    static init(context: ExtensionContext): void {
        /*
        Create instance of new AuthSettings.
        */
        AuthSettings._instance = new AuthSettings(context.secrets);
    }

    static get instance(): AuthSettings {
        /*
        Getter of our AuthSettings existing instance.
        */
        return AuthSettings._instance;
    }

    async storeAuthData(key: string | undefined, token: string | undefined): Promise<void> {
        /*
        Update values in bugout_auth secret storage.
        */
        if (key && token) {
            return this.secretStorage.store(key, token);
        }
    }

    async getAuthData(key: string | undefined): Promise<string | undefined> {
        /*
        Retrieve data from secret storage.
        */
        if (key) {
            return await this.secretStorage.get(key);
        } else {
            return "";
        }
    }

    async removeAuthData(key: string | undefined): Promise<void> {
        /*
        Remove data from secret storage.
        */
        if (key) {
            return await this.secretStorage.delete(key);
        }
    }
}
