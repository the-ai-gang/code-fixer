# Code Fixer

Welcome to Code Fixer, a Visual Studio Code extension that uses the OpenAI's edits API to help you write code faster. With Code Fixer, you can get assistance while coding and generate nice chunks of code, directly from your favorite IDE.

# Installation

- Open Visual Studio Code
- Select the "Extensions" panel on the left-hand side
- Search for "Code Fixer"
- Click "Install"
- Open the Command Palette (Ctrl+Shift+P) and type "Code Fixer: Set Token" to set your Open AI API Token ([generate here](https://beta.openai.com/account/api-keys))



# Usage



**To use Code Fixer, open the file you would like to get assisted on.**

**Then use the "Code Fixer: Prompt" command in the Command Palette.**

**You can also highlight a block of code and run the same command, then only the highlighted code will be sent to GPT.**

Code Fixer will generate new code based on the current file or selected block, allowing you to write faster and more efficiently.


# Credits

Code Fixer is powered by the OpenAI's edits API, which uses advanced machine learning techniques to assist with code writing. We would like to thank the OpenAI team for their contributions to this project.


# License

Code Fixer is released under the MIT License. See the LICENSE file for more information.

# Disclaimer

This readme was generated with the help of ChatGPT ;)